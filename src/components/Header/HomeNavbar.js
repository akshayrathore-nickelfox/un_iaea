import React from "react";
import Link from "next/link";
import Image from "next/image";
import style from "../../styles/homeheader.module.css";
import HomeSubHeader from "./HomeSubHeader";



function HomeNavbar() {

  return (
    <div>
      <HomeSubHeader />
      <div className={style.home_main_header}>
      <div className={style.home_logo_wrapper}>
          <div className={style.home_header_logo}>
            <Link href="/" passHref>
              <a>
              <Image
                src="/assets/images/header/DetailedLogo.svg"
                width="260"
                height="120"
                alt="logo"
              />
              </a>
            </Link>
          </div>
        </div>
        <div className={style.home_header_content}>
          <nav className={style.home_navigation_container}>
            <ul>
              <li className={style.home_section}>
              <Link href="/">Home</Link>
            </li>
            <li>
              <Link href="/technical-data">Technical</Link>
            </li>
            <li>
              <Link href="/deployment">Deployment</Link>
            </li>
            <li>
              <Link href="/deployment">Characteristics</Link>
            </li>
            <li>
              <Link href="/publications">Publications</Link>
            </li>
            <li>
              <Link href="/glossary">Glossary</Link>
            </li>
            <li>
              <Link href="/about">About</Link>
            </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}

export default HomeNavbar;
