import React, { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import style from "../../styles/header.module.css";
import SubHeader from "./SubHeader";

function Navbar() {
  const [toggle, setToggle] = useState({
    count: 0,
  });
  const navigationActiveObj = [
    {
      category: "Home",
      link: "/",
    },
    {
      category: "Technical Data",
      link: "/technical-data",
    },
    {
      category: "Deployment",
      link: "/deployment",
    },
    {
      category: "Characteristics",
      link: "/characteristics",
    },
    {
      category: "Publications",
      link: "/publications",
    },
    {
      category: "Glossary",
      link: "/glossary",
    },
    {
      category: "About",
      link: "/about",
    },
  ];
  return (
    <div className={style.main_header}>
      <div className={style.header_logo}>
        <Link href="/" passHref={true}>
          <a>
            <Image
              src="/assets/images/header/DetailedLogo.svg"
              width="260"
              height="120"
              alt="logo"
            />
          </a>
        </Link>
      </div>
      <div className={style.header_search_content}>
        <SubHeader />
        <nav className={style.navigation_container}>
          <ul>
            {navigationActiveObj.map((data, index) => (
              <li
                className={toggle.count === index ? style.activeLink : null}
                key={index}
              >
                <Link href={data.link}>
                  <a
                    onClick={() => {
                      setToggle({
                        count: index,
                      });
                    }}
                  >
                    {data.category}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </div>
  );
}

export default React.memo(Navbar);
